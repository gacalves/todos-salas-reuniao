﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Api.Dtos;
using Todos.Agenda.Api.Messages;
using Todos.Agenda.Application.Evento.Atualizar;
using Todos.Agenda.Application.Evento.Inserir;
using Todos.Agenda.Application.Evento.Remover;
using Todos.Agenda.Application.PipeLines;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Api.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class EventoController : ControllerBase {
        private readonly IEventoRead eventoRead;
        private readonly IMediator mediator;

        public EventoController(IEventoRead eventoRead, IMediator mediator) {
            this.eventoRead = eventoRead;
            this.mediator = mediator;
        }

        /// <summary>
        /// Mapea os dados da entidade de dominio para o DTO.
        /// </summary>
        private EventoDto ToDto(Evento evento) {
            return new EventoDto() {
                Descricao = evento.Descricao,
                EventoId = evento.Id,
                Fim = evento.Fim,
                Inicio = evento.Inicio,
                Responsavel = evento.Responsavel,
                Sala = new SalaDto() { Nome = evento.Sala.Nome, SalaId = evento.Sala.Id }
            };
        }

        
        [HttpGet("Sala/{salaId}")]
        public async Task<Response> GetPorSala(Guid salaId) {
            try {
                var eventos = await eventoRead.ObterEventosPorSala(salaId);
                return HttpMessages.Ok("Consulta efetuada com sucesso.", eventos.Select(e => ToDto(e)));
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Não foi possível recuperar os eventos para a sala informada!", new string[] { ex.Message });
            }
        }

        [HttpGet("{eventoId}")]
        public async Task<Response> Get(Guid eventoId) {
            try {
                var evento = await eventoRead.ObterEventoPorId(eventoId);
                return HttpMessages.Ok("Consulta efetuada com sucesso.", ToDto(evento));
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Não foi possível recuperar o evento informado!", new string[] { ex.Message });
            }
        }

        [HttpPost]
        public async Task<Response> Post(CriarEventoCommand command) {
            try {
                var result = await mediator.Send(command, CancellationToken.None);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Dados inválidos!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Evento criado com sucesso!");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha ao criar o evento!", new string[] { ex.Message });
            }
        }

        [HttpPut]
        public async Task<Response> Put(AtualizarEventoCommand command) {
            try {
                var result = await mediator.Send(command);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Dados inválidos!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Evento atualizado com sucesso!");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha ao atualizar o evento!", new string[] { ex.Message });
            }
        }

        [HttpDelete("{eventoId}")]
        public async Task<Response> Delete(Guid eventoId) {
            try {
                var command = new CancelarEventoCommand() { EventoId = eventoId };
                var result = await mediator.Send(command);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Dados inválidos!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Evento cancelado com sucesso!");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha ao cancelar o evento!", new string[] { ex.Message });
            }
        }
    }
}
