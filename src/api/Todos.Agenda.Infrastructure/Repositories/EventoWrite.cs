﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;

namespace Todos.Agenda.Infrastructure.Repositories {
    public class EventoWrite : IEventoWritre {


        private AgendaDbContext context;
        public EventoWrite(AgendaDbContext context) {
            this.context = context;
        }

        public async Task<Evento> Atualizar(Evento evento, CancellationToken cancellationToken) {
            var localEntity = context.Eventos.Local.Where(x => x.Id == evento.Id).FirstOrDefault();
            if (localEntity != null) {
                context.Entry(localEntity).State = EntityState.Detached;
            }
            var entry = context.Eventos.Update(evento);
            await context.SaveChangesAsync(cancellationToken);
            return evento;
        }

        public async Task<Evento> Inserir(Evento evento, CancellationToken cancellationToken) {
            var entry = context.Eventos.Add(evento);
            await context.SaveChangesAsync(cancellationToken);
            var eventoInserido = new Evento(entry.Entity.Descricao, entry.Entity.Responsavel, entry.Entity.Inicio, entry.Entity.Fim, evento.Sala, entry.Entity.Id);
            return eventoInserido;
        }

        public async Task<Evento> Remover(Guid eventoId, CancellationToken cancellationToken) {
            var entitdadeARemover = context.Eventos.FirstOrDefault(x => x.Id == eventoId);
            if (entitdadeARemover != null) {
                context.Eventos.Remove(entitdadeARemover);
                await context.SaveChangesAsync(cancellationToken);
            }
            return entitdadeARemover;
        }
    }
}
