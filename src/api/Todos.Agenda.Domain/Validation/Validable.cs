﻿using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todos.Agenda.Domain.Validation {
    public abstract class Validable : Notifiable, IValidatable {
        public abstract void Validate();
    }
}
