import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoCriarComponent } from './evento-criar.component';

describe('EventoCriarComponent', () => {
  let component: EventoCriarComponent;
  let fixture: ComponentFixture<EventoCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
