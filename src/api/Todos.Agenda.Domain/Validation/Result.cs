﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todos.Agenda.Domain.Validation {
    public class Result {
        private List<string> _validations;

        public Result() {
            _validations = new List<string>();
        }

        public static Result Ok = new Result();
        public bool HasValidation => _validations.Count > 0;
        public IList<string> Validations => _validations;
        public void AddValidation(string validation)
            => _validations.Add(validation);
       
    }

}
