﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;

namespace Todos.Agenda.Infrastructure.Repositories {
    public class SalaWrite : ISalaWrite {

        private AgendaDbContext context;
        public SalaWrite(AgendaDbContext context) {
            this.context = context;
        }

        public async Task<Sala> Atualizar(Sala sala, CancellationToken cancellationToken) {
            var localEntity = context.Salas.Local.Where(x => x.Id == sala.Id).FirstOrDefault();
            if (localEntity != null) {
                context.Entry(localEntity).State = EntityState.Detached;
            }
            var entry = context.Salas.Update(sala);
            await context.SaveChangesAsync(cancellationToken);
            return sala;
        }

        public async Task<Sala> Inserir(Sala sala, CancellationToken cancellationToken) {
            var salaInserida = await context.Salas.AddAsync(sala, cancellationToken);
            context.SaveChanges();
            return salaInserida.Entity;
        }

        public async Task<Sala> Remover(Guid salaId, CancellationToken cancellationToken) {
            var entitdadeARemover = context.Salas.FirstOrDefault(x => x.Id == salaId);
            if (entitdadeARemover != null) {
                context.Salas.Remove(entitdadeARemover);
                await context.SaveChangesAsync(cancellationToken);
            }
            return entitdadeARemover;
        }

    }
}
