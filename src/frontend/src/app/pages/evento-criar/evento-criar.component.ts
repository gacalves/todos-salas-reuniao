import { Component, OnInit } from "@angular/core";
import { Evento } from "../../models/evento";
import { EventoService } from "../../services/evento.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Sala } from "../../models/sala";

@Component({
  selector: "app-evento-criar",
  templateUrl: "./evento-criar.component.html",
  styleUrls: ["./evento-criar.component.css"],
})
export class EventoCriarComponent implements OnInit {
  eventoId: string;
  descricao: string;
  inicio;
  fim;
  salaId: string;
  responsavel: string;

  editando: boolean = false;

  constructor(
    private eventoService: EventoService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.salaId = localStorage.getItem("salaId");
    this.eventoId = history.state.eventoId;

    if (this.eventoId) {
      this.eventoService
        .obterEventoPorId(this.eventoId)
        .subscribe((resultado) => {
          console.log(resultado);
          if (resultado.success) {
            this.editando = true;
            const e = resultado.data;
            this.descricao = e.descricao;
            this.inicio = e.inicio;
            this.fim = e.fim;
            this.responsavel = e.responsavel;
            this.salaId = e.sala.salaId;
          }
        });
    } else {
      this.editando = false;
    }
  }

  salvarEvento() {
    let model: Evento = {
      descricao: this.descricao,
      inicio: this.inicio,
      fim: this.fim,
      salaId: this.salaId,
      responsavel: this.responsavel,
      eventoId: this.eventoId,
      sala: null,
    };

    if (this.eventoId) {
      this.eventoService.editarEvento(model).subscribe((resultado) => {
        if (resultado.success) {
          this.router.navigate(["calendario"]);
        } else {
          var messsages = resultado.validationErrors.join("\n");
          this.openSnackBar(`${resultado.message}\n${messsages}`);
        }
      });
    } else {
      this.eventoService.criarEvento(model).subscribe((resultado) => {
        if (resultado.success) {
          this.router.navigate(["calendario"]);
        } else {
          var messsages = resultado.validationErrors.join("\n");
          this.openSnackBar(`${resultado.message}\n${messsages}`);
        }
      });
    }
  }

  cancelarEvento() {
    this.eventoService.cancelarEvento(this.eventoId).subscribe((resultado) => {
      if (resultado.success) {
        this.openSnackBar("Reunião cancelada com sucesso!");
        setTimeout(() => {
          this.router.navigate(["calendario"], { replaceUrl: true });
        }, 3000);
      } else {
        var messsages = resultado.validationErrors.join("\n");
        this.openSnackBar(`${resultado.message}\n${messsages}`);
      }
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, "", {
      duration: 5000,
    });
  }
}
