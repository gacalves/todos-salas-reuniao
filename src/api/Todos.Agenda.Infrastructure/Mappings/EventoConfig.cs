﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Todos.Agenda.Domain.Entities;

namespace Todos.Agenda.Infrastructure.Mappings {
    
    class EventoConfig : IEntityTypeConfiguration<Evento> {
        public void Configure(EntityTypeBuilder<Evento> builder) {

            builder.ToTable("Evento");
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Descricao);
            builder.Property(s => s.Responsavel);
            builder.Property(s => s.Inicio);
            builder.Property(s => s.Fim);

            builder.HasOne(t => t.Sala);

            builder.Ignore(x => x.Invalid);
            builder.Ignore(x => x.Valid);
            builder.Ignore(x => x.Notifications);
        }
    }
}
