﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.PipeLines {
    public class ValidateCommand<TRequest,TResponse> : IPipelineBehavior<TRequest, TResponse> where TResponse : Result{

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next) {
            if (request is Validable validatable) {
                validatable.Validate();
                if (validatable.Invalid) {
                    var validations = new Result();
                    foreach (Flunt.Notifications.Notification notification in validatable.Notifications)
                        validations.AddValidation(notification.Message);
                    return validations as TResponse;
                }
            }

            var result = await next();
            return result;
        }
    }
}
