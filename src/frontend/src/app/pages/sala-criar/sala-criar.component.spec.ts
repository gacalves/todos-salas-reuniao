import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaCriarComponent } from './sala-criar.component';

describe('SalaCriarComponent', () => {
  let component: SalaCriarComponent;
  let fixture: ComponentFixture<SalaCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
