﻿using Microsoft.EntityFrameworkCore;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Infrastructure.Mappings;

namespace Todos.Agenda.Infrastructure {
    public class AgendaDbContext : DbContext {
        public virtual DbSet<Sala> Salas { get; set; }
        public virtual DbSet<Evento> Eventos { get; set; }

        public AgendaDbContext(DbContextOptions<AgendaDbContext> options)
            : base(options) {
        }

        public AgendaDbContext() { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.ApplyConfiguration(new SalaConfig());
            modelBuilder.ApplyConfiguration(new EventoConfig());
        }

    }
}
