﻿using System.Collections.Generic;
using Todos.Agenda.Api.Dtos;

namespace Todos.Agenda.Api.Messages {
    public static class HttpMessages {
        public static Response Ok(string message = "", dynamic data = null) {
            return new Response() { Success = true, Message = message, Data = data, ValidationErrors = { } };
        }

        public static Response Failure(string message, IList<string> validationErros, IDto data = null) {
            return new Response() { Success = false, Message = message, Data = data, ValidationErrors = validationErros };
        }
    }

    public class Response {
        public bool Success { get; set; }
        public string Message { get; set; }
        public IList<string> ValidationErrors { get; set; }
        public dynamic Data { get; set; }

    }
}
