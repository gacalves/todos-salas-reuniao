﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Todos.Agenda.Domain.Entities;

namespace Todos.Agenda.Infrastructure.Mappings {
    class SalaConfig : IEntityTypeConfiguration<Sala> {
        public void Configure(EntityTypeBuilder<Sala> builder) {

            builder.ToTable("Sala");
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Nome);

            builder.Ignore(x => x.Invalid);
            builder.Ignore(x => x.Valid);
            builder.Ignore(x => x.Notifications);
        }
    }
}
