﻿using Flunt.Validations;
using MediatR;
using System;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Evento.Remover {
    public class CancelarEventoCommand : Validable, IRequest<Result> {
        public Guid EventoId { get; set; }

        public override void Validate() {
            AddNotifications(new Contract()
                .IsNotNull(EventoId, nameof(EventoId), "Evento inválido!")
                .AreNotEquals(EventoId, default, nameof(EventoId), "Evento inválido!"));
        }
    }
}
