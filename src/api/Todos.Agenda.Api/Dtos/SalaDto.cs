﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Todos.Agenda.Api.Dtos {
    public class SalaDto : IDto {
        public Guid SalaId { get; set; }
        public string Nome { get; set; }
    }
}
