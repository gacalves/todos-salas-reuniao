﻿using Flunt.Notifications;
using Flunt.Validations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Commands.Sala.Inserir {
    public class CriarSalaCommand : Validable, IRequest<Result> {

        public string Nome { get; set; }

        public override void Validate() {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(Nome, nameof(Nome), "O nome da sala deve conter entre 1 e 250 caracteres")
                .HasMaxLen(Nome, 250, nameof(Nome), "O nome da sala deve no máximo 250 caracteres."));
        }
    }
}
