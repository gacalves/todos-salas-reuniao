﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Evento.Remover {
    class CancelarEventoHandler : IRequestHandler<CancelarEventoCommand, Result> {

        private readonly IEventoWritre eventoWrite;
        private readonly IEventoRead eventoRead;

        public CancelarEventoHandler(IEventoWritre eventoWrite, IEventoRead eventoRead) {
            this.eventoWrite = eventoWrite;
            this.eventoRead = eventoRead;
        }

        public async Task<Result> Handle(CancelarEventoCommand request, CancellationToken cancellationToken) {
            var result = new Result();
            try {
                var evento = await eventoRead.ObterEventoPorId(request.EventoId);
                if (evento == null) {
                    result.AddValidation("Evento não encontrado!");
                }
                else {
                    await eventoWrite.Remover(request.EventoId, cancellationToken);
                    result = new Result();
                }
            }
            catch (Exception) {
                result.AddValidation("Houve uma falha ao remover o evento!");
            }
            return result;
        }
    }
}
