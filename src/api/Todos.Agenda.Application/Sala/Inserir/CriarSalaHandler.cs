﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Application.Commands;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Services;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Commands.Sala.Inserir {
    public class CriarSalaHandler : IRequestHandler<CriarSalaCommand, Result> {

        private readonly IMediator mediator;
        private readonly ISalaRead salaRead;
        private readonly ISalaWrite salaWrite;

        public CriarSalaHandler(IMediator mediator, ISalaWrite salaWrite, ISalaRead salaRead) {
            this.mediator = mediator;
            this.salaRead = salaRead;
            this.salaWrite = salaWrite;
        }

        public async Task<Result> Handle(CriarSalaCommand request, CancellationToken cancellationToken) {
            return await SalaService.CriarSalaAsync(request.Nome, salaWrite, salaRead, cancellationToken);
        }
    }
}
