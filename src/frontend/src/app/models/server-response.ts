export interface ServerResponse<TData>{
  success:string;
  message:string;
  validationErrors:string[];
  data:TData;
}