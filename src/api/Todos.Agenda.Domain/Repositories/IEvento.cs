﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;

namespace Todos.Agenda.Domain.Repositories {
    public interface IEventoRead {

        /// <summary>
        /// Retorna a quantidade de eventos que fazem interseção com o período informado, identificada por SalaId.
        /// </summary>
        Task<int> ContarEventosPorPeriodo(Guid SalaId, DateTime inicio, DateTime fim);

        /// <summary>
        /// Retorna a quantidade de eventos que fazem interseção com o período informado, identificada por SalaId, excedo o evento com o definido em exceto.
        /// </summary>
        Task<int> ContarEventosPorPeriodo(Guid SalaId, DateTime inicio, DateTime fim, Guid exceto);

        /// <summary>
        /// Recupera um evento pelo id.
        /// </summary>
        Task<Evento> ObterEventoPorId(Guid eventoId);

        /// <summary>
        /// Recupera todos os eventos da sala.
        /// </summary>
        Task<IEnumerable<Evento>> ObterEventosPorSala(Guid salaId);
    }

    public interface IEventoWritre {
        /// <summary>
        /// Insere um novo evento no armazenamento. 
        /// O Identificador é gerado aqui.
        /// </summary>
        Task<Evento> Inserir(Evento evento, CancellationToken cancellationToken);

        /// <summary>
        /// Atualiza um evento no armazenamento.
        /// </summary>
        Task<Evento> Atualizar(Evento evento, CancellationToken cancellationToken);

        /// <summary>
        /// Remove o evento do armazenamento.
        /// </summary>
        Task<Evento> Remover(Guid EventoId, CancellationToken cancellationToken);
    }
}
