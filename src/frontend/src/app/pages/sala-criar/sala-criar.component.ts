import { Component, OnInit } from "@angular/core";
import { SalaService } from "../../services/sala.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-sala-criar",
  templateUrl: "./sala-criar.component.html",
  styleUrls: ["./sala-criar.component.css"],
})
export class SalaCriarComponent implements OnInit {
  salaId: string;
  nomeSala: string = "";
  editando: boolean = false;

  constructor(
    private salaService: SalaService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.salaId = history.state.salaId;
    if (this.salaId) {
      this.salaService.obterSalaPorId(this.salaId).subscribe((resultado) => {
        if (resultado.success) {
          this.nomeSala = resultado.data.nome;
          this.editando = true;
        }
      });
    } else {
      this.editando = false;
    }
  }

  salvarSala() {
    if (this.salaId) {
      this.salaService
        .editarSala({ salaId: this.salaId, nome: this.nomeSala })
        .subscribe((resultado) => {
          if (resultado.success) {
            this.router.navigate(["sala-lista"], { replaceUrl: true });
          } else {
            var messsages = resultado.validationErrors.join("\n");
            this.openSnackBar(`${resultado.message}\n${messsages}`);
          }
        });
    } else {
      this.salaService
        .criarSala({ salaId: null, nome: this.nomeSala })
        .subscribe((resultado) => {
          if (resultado.success) {
            this.router.navigate(["sala-lista"], { replaceUrl: true });
          } else {
            var messsages = resultado.validationErrors.join("\n");
            this.openSnackBar(`${resultado.message}\n${messsages}`);
          }
        });
    }
  }

  removerSala() {
    this.salaService.removerSala(this.salaId).subscribe((resultado) => {
      if (resultado.success) {
        this.openSnackBar("Sala removida com sucesso!");
        setTimeout(() => {
          this.router.navigate(["sala-lista"], { replaceUrl: true });
        }, 3000);
      } else {
        var messsages = resultado.validationErrors.join("\n");
        this.openSnackBar(`${resultado.message}\n${messsages}`);
      }
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, "", {
      duration: 5000,
    });
  }
}
