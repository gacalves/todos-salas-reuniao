import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ServerResponse } from "../models/server-response";
import { Sala } from "../models/sala";
import { Observable } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
  }),
};

@Injectable({
  providedIn: "root",
})
export class SalaService {
  constructor(private http: HttpClient) {}

  obterSalas(): Observable<ServerResponse<Sala[]>> {
    return this.http.get<ServerResponse<Sala[]>>(
      environment.serviceBaseUrl + "/Sala"
    );
  }

  obterSalaPorId(salaId: string): Observable<ServerResponse<Sala>> {
    return this.http.get<ServerResponse<Sala>>(
      `${environment.serviceBaseUrl}/Sala/${salaId}`
    );
  }

  criarSala(sala:Sala): Observable<ServerResponse<any>> {
    return this.http.post<ServerResponse<any>>(
      environment.serviceBaseUrl + "/Sala",
      sala,
      httpOptions
    );
  }

  editarSala(sala:Sala): Observable<ServerResponse<any>> {
    return this.http.put<ServerResponse<any>>(
      environment.serviceBaseUrl + "/Sala",
      sala,
      httpOptions
    );
  }

  removerSala(salaId: string) {
    return this.http.delete<ServerResponse<any>>(
      `${environment.serviceBaseUrl}/Sala/${salaId}`,
      httpOptions
    );
  }
}
