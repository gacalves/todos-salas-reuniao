﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;

namespace Todos.Agenda.Domain.Repositories {

    /// <summary>
    /// Interface para o repositório de leitura de salas.
    /// </summary>
    public interface ISalaRead {

        /// <summary>
        /// Conta quantas salas tem exatamente o mesmo nome.
        /// </summary>
        Task<int> ContarSalasPorNome(string nome);

        /// <summary>
        /// Recupera as sala com o mesmo nome.
        /// </summary>
        Task<List<Sala>> ObterSalasPorNome(string nome);

        /// <summary>
        /// Busca todas as salas cadastradas.
        /// </summary>
        Task<List<Sala>> ObterSalas();

        /// <summary>
        /// Retorna a sala pelo Id.
        /// </summary>
        Task<Sala> ObterSalaPorId(Guid SalaId);

        /// <summary>
        /// Retorna a quantidade de eventos registrados para a sala.
        /// </summary>
        Task<int> ContarEventosPorSala(Guid SalaId);
    }

    /// <summary>
    /// Interface para o respositório de alteração de dados das salas.
    /// </summary>
    public interface ISalaWrite {

        /// <summary>
        /// Insere um novo registro de sala no armazenamento. 
        /// O Identificador é gerado aqui.
        /// </summary>
        Task<Sala> Inserir(Sala sala, CancellationToken cancellationToken);

        /// <summary>
        /// Atualiza um registro de sala no armazenamento.
        /// </summary>
        Task<Sala> Atualizar(Sala sala, CancellationToken cancellationToken);

        /// <summary>
        /// Remove o registro da sala no armazenamento.
        /// Pode-se configurar a exclusão lógica, embora isto deve ser considerado nas consultas.
        /// </summary>
        Task<Sala> Remover(Guid salaId, CancellationToken cancellationToken);

    }
}
