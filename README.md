**Todos - Salas de Reunião**

Aplicação destinada a agendamento de reuniões em salas físicas.

---

## API - Padrões utilizados

**CQRS - Command Query Responsibility Segregation**

Este padrão traz a possibilidade de se utilizar modelos de leitura diferentes dos modelos de atualização de dados.
Além de possibilitar menor acoplamento entre as camadas da aplicação, o CQRS traz o benefício de se poder utilizar modelos de leitura mais performáticos (por exemplo: bancos não relacionais).
Neste projeto foi utilizado o MediatR e configurado o Pipeline Behavior para executar validações dos commands antes de chegarem a camada de domínio.


**DDD - Domain Driven Design**

Conjunto de técnicas de modelagem para softwares complexo que permite focar no domínio do cliente, modelando software curado e o mais desacoplado possível dos componentes de infraestrutura.
O domínio desta aplicação é extremamentes simples embora, visando seguir as melhores práticas do DDD foram criados os serviços de domínio e aplicação quando necessário.


**Infraestrutura**

Toda a aplicação foi construída baseada no .net core 3.1. Na implementação da camada de persistência é utilizado Entity Framework Core 3.1.3.

---

## Frontend

Na construção do camada de apresentação foi utilizado Angular 9, com o framework de componentes Angular Material.

---

**Instruções**

Para executar a aplicação deve-se ter o angular CLI >= 9 e no .NET Core 3.1 SDK instalados. 
Entre na pasta src/frontend e execute os comandos:

```sh
 npm i
 npm start
```


Entre na pasta src/api e execute os comandos:

```sh
 cd Todos.Agenda.Api
 dotnet build
 dotnet run
```


Caso seja alterado endereço da API, o mesmo deve ser configurado em src/frontend/src/environments/environments.ts

---

**Observações**

Alguns dos padrões utilizados aqui não são necessários a aplicações deste porte, foram utilizados somente como prova de conhecimento.

Não foi implementada a camada segurança. Para um sistema com esta arquitetura é recomendado um serviço de autenticação que suporte as especificações do OpenID Connect/OAuth (por exemplo IdentityServer4).

---

**Links úteis**

1. [DDD](https://martinfowler.com/tags/domain%20driven%20design.html)
2. [CQRS](https://martinfowler.com/bliki/CQRS.html)
3. [OpenID Connect](https://openid.net/connect/)
