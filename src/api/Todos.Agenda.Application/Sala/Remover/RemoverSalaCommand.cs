﻿using Flunt.Validations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Sala.Remover {
    public class RemoverSalaCommand : Validable, IRequest<Result> {

        public Guid SalaId { get; set; }
        public override void Validate() {
            AddNotifications(new Contract().IsNotEmpty(SalaId, nameof(SalaId), "Sala inválida!"));
        }
    }
}
