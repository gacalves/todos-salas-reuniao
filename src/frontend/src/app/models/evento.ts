import { Sala } from './sala';
export interface Evento {
  descricao: string;
  responsavel: string;
  inicio: string;
  fim: string;
  salaId: string;
  eventoId:string;
  sala:Sala;
}
