import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import { Calendar } from "@fullcalendar/core";
import ptbrLocale from "@fullcalendar/core/locales/pt-br";
import { EventoService } from "../../services/evento.service";
import { Router } from "@angular/router";
import { Evento } from "src/app/models/evento";
import { EventSourceInput } from "@fullcalendar/core/structs/event-source";
import { SalaService } from '../../services/sala.service';

@Component({
  selector: "app-calendario",
  templateUrl: "./calendario.component.html",
  styleUrls: ["./calendario.component.css"],
})
export class CalendarioComponent implements OnInit {
  @ViewChild("fullCalendar", { static: false }) calendarEl: ElementRef;

  calendarPlugins = [dayGridPlugin];
  salaId: string;
  nomeSala:string;
  eventos: EventSourceInput[];

  constructor(private eventoService: EventoService, private salaService:SalaService,  private router: Router) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.salaId = localStorage.getItem("salaId");

    this.eventoService
      .obterEventosPorSala(this.salaId)
      .subscribe((resposta) => {
        this.eventos = resposta.data.map((e) => {
          return {
            id: e.eventoId,
            title: e.descricao,
            start: e.inicio,
            end: e.fim,
          };
        });

        const canlendarContainer = this.calendarEl.nativeElement;

        let calendar = new Calendar(canlendarContainer, {
          plugins: [dayGridPlugin],
          locale: ptbrLocale,
          events: this.eventos,
          eventClick: (info) => {
            this.router.navigate(["evento-editar"], {
              state: { eventoId: info.event.id },
            });
            info.el.style.borderColor = "red";
          },
        });

        calendar.render();
      });

      this.salaService.obterSalaPorId(this.salaId).subscribe(resultado=>{
        if(resultado.success){
          this.nomeSala=resultado.data.nome;
        }
      })
  }

  novoEvento() {
    this.router.navigate(["evento-criar"]);
  }

}
