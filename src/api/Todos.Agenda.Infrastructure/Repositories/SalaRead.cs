﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;

namespace Todos.Agenda.Infrastructure.Repositories {
    public class SalaRead : ISalaRead {

        private readonly AgendaDbContext context;
        public SalaRead(AgendaDbContext dbcontext) {
            context = dbcontext;
        }

        public async Task<int> ContarEventosPorSala(Guid salaId) {
            return await context.Eventos.Where(x => x.Sala.Id == salaId).CountAsync();
        }

        public async Task<int> ContarSalasPorNome(string nome) {
            var quantidade = await context.Salas.CountAsync(x => x.Nome == nome && x.Ativa);
            return quantidade;
        }


        public async Task<Sala> ObterSalaPorId(Guid salaId) {
            return await context.Salas.FirstOrDefaultAsync(x => x.Id == salaId);
        }

        public async Task<List<Sala>> ObterSalas() {
            var salas =  await context.Salas.Where(x=>x.Ativa).ToListAsync();
            return salas;
        }

        public async Task<List<Sala>> ObterSalasPorNome(string nome) {
            return await context.Salas.Where(x => x.Nome == nome && x.Ativa).ToListAsync();
        }
    }
}
