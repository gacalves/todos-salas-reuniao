﻿using Flunt.Validations;
using MediatR;
using System;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Evento.Atualizar {
    public class AtualizarEventoCommand : Validable, IRequest<Result> {
        public Guid EventoId { get;  set; }
        public string Descricao { get;  set; }
        public string Responsavel { get;  set; }
        public DateTime Inicio { get;  set; }
        public DateTime Fim { get;  set; }
        public Guid SalaId { get; set; }

        public override void Validate() {
            AddNotifications(new Contract()
                .IsNotNull(EventoId, nameof(EventoId), "Evento inválido!")
                .AreNotEquals(EventoId, default, nameof(EventoId), "Evento inválido!")
                .IsNotNullOrEmpty(Descricao, nameof(Descricao), "A descrição do evento deve ser informada!")
                .HasMaxLen(Descricao, 250, nameof(Descricao), "A descrição do evento deve conter no máximo 250 caracteres!")
                .IsNotNullOrEmpty(Responsavel, nameof(Responsavel), "O nome do responsável pelo evento deve ser informado!")
                .HasMaxLen(Responsavel, 250, nameof(Responsavel), "O nome do responsável pelo evento deve conter no máximo 250 caracteres!")
                .IsGreaterThan(Inicio, DateTime.Now, nameof(Inicio), "A data de iníco do evento deve ser no futuro!")
                .IsGreaterThan(Fim, Inicio, nameof(Fim), "A data final do evento deve ser superior ao início!")
                .IsNotNull(SalaId, nameof(SalaId), "Sala inválida!")
                .AreNotEquals(SalaId, default, nameof(SalaId), "Sala inválida!")
                );
        }
    }
}
