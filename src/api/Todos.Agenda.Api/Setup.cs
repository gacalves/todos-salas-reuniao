﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Todos.Agenda.Application.Commands.Sala.Inserir;
using Todos.Agenda.Application.PipeLines;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Validation;
using Todos.Agenda.Infrastructure;

namespace Todos.Agenda.Api {
    public static class Setup {

        public static void ConfigureRepositories(this IServiceCollection services, IConfiguration configuration) {

            services.AddDbContext<AgendaDbContext>(options =>
                options.UseSqlite(configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient(typeof(ISalaRead), typeof(Infrastructure.Repositories.SalaRead));
            services.AddTransient(typeof(ISalaWrite), typeof(Infrastructure.Repositories.SalaWrite));
            services.AddTransient(typeof(IEventoRead), typeof(Infrastructure.Repositories.EventoRead));
            services.AddTransient(typeof(IEventoWritre), typeof(Infrastructure.Repositories.EventoWrite));
        }

        public static void ConfigureMediatR(this IServiceCollection services) {
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidateCommand<,>));
            services.AddMediatR(typeof(CriarSalaCommand).GetTypeInfo().Assembly);
        }
    }
}
