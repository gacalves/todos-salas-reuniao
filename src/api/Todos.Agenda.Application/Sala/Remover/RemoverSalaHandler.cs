﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Application.Commands.Sala.Inserir;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Services;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Sala.Remover {
    public class RemoverSalaHandler : IRequestHandler<RemoverSalaCommand, Result> {

        private readonly ISalaRead salaRead;
        private readonly ISalaWrite salaWrite;
        private readonly IMediator mediator;

        public RemoverSalaHandler(ISalaWrite salaWrite, ISalaRead salaRead, IMediator mediator) {
            this.salaWrite = salaWrite;
            this.salaRead = salaRead;
            this.mediator = mediator;
        }

        public async Task<Result> Handle(RemoverSalaCommand request, CancellationToken cancellationToken) {
            return await SalaService.RemoverSalaAsync(salaRead, salaWrite, request.SalaId, cancellationToken);
        }
    }
}
