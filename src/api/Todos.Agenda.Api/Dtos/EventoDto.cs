﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Todos.Agenda.Api.Dtos {
    public class EventoDto : IDto {
        public Guid EventoId { get; set; }
        public string Descricao { get; set; }
        public string Responsavel { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public SalaDto Sala { get; set; }
        public Guid SalaId { get; set; }
    }
}
