﻿using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todos.Agenda.Domain.Entities {
    public class Evento : Notifiable {

        public Guid Id { get; private set; }
        public string Descricao { get; private set; }
        public string Responsavel { get; private set; }
        public DateTime Inicio { get; private set; }
        public DateTime Fim { get; private set; }
        public Sala Sala { get; private set; }

        public Evento(string descricao, string responsavel, DateTime inicio, DateTime fim, Sala sala, Guid id = default) {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(descricao, nameof(descricao), "A descrição do evento deve informado!")
                .HasMaxLen(descricao, 250, nameof(descricao), "A descrição do evento deve conter no máximo 250 caracteres!")
                .IsNotNullOrEmpty(responsavel, nameof(responsavel), "O nome do responsável pelo evento deve ser informado!")
                .HasMaxLen(responsavel, 250, nameof(responsavel), "O nome do responsável pelo evento deve conter no máximo 250 caracteres!")
                .IsGreaterThan(inicio, DateTime.Now, nameof(inicio), "A data de iníco do evento deve ser no futuro!")
                .IsGreaterThan(fim, inicio, nameof(fim), "A data final do evento deve ser superior ao início!")
                .IsNotNull(sala, nameof(sala), "É preciso informar a sala onde ocorrerá o evento!")
                );

            Descricao = descricao;
            Responsavel = responsavel;
            Inicio = inicio;
            Fim = fim;
            Sala = sala;
            Id = id;
        }
        public Evento() {

        }
    }
}
