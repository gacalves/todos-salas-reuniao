﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Domain.Services {
    public static class EventoService {

        public static async Task<Result> CriarEventoAsync(string descricao, string responsavel, DateTime inicio, DateTime fim, Guid salaId,
            IEventoRead eventoRead, IEventoWritre eventoWrite, ISalaRead salaRead, CancellationToken cancellationToken) {


            var sala = await salaRead.ObterSalaPorId(salaId);
            var validations = new Result();

            if (sala == null) {
                validations.AddValidation("Sala inválida!");
                return validations;
            }

            var qtdEventosMesmoPeriodo = await eventoRead.ContarEventosPorPeriodo(sala.Id, inicio, fim);
            if (qtdEventosMesmoPeriodo > 0) {
                validations.AddValidation("O período informado entra em conflito com outros eventos! Verifique o agendamento da sala.");
                return validations;
            }

            var evento = new Evento(descricao, responsavel, inicio, fim, sala);

            if (evento.Valid) {
                await eventoWrite.Inserir(evento, cancellationToken);
                return new Result();
            }
            else {
                foreach (var error in evento.Notifications) {
                    validations.AddValidation(error.Message);
                }
                return validations;
            }
        }

        public static async Task<Result> AtualizarEventoAsync(Guid eventoId, string descricao, string responsavel, DateTime inicio, DateTime fim, Guid salaId, IEventoRead eventoRead, IEventoWritre eventoWrite, ISalaRead salaRead, CancellationToken cancellationToken) {

            var validations = new Result();

            var sala = await salaRead.ObterSalaPorId(salaId);
            var evento = await eventoRead.ObterEventoPorId(eventoId);

            if (sala == null) {
                validations.AddValidation("Sala inválida!");
                return validations;
            }

            if (evento == null) {
                validations.AddValidation("Evento inválido!");
                return validations;
            }

            var qtdEventosMesmoPeriodo = await eventoRead.ContarEventosPorPeriodo(sala.Id, inicio, fim, eventoId);
            if (qtdEventosMesmoPeriodo > 0) {
                validations.AddValidation("O período informado entra em conflito com outros eventos! Verifique o agendamento da sala.");
                return validations;
            }

            var eventoAtualizado = new Evento(descricao, responsavel, inicio, fim, sala, eventoId);

            if (eventoAtualizado.Valid) {
                await eventoWrite.Atualizar(eventoAtualizado, cancellationToken);
                return new Result();
            }
            else {
                foreach (var error in eventoAtualizado.Notifications) {
                    validations.AddValidation(error.Message);
                }
                return validations;
            }
        }
    }
}
