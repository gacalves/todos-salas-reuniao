import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import * as moment from "moment";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatListModule } from "@angular/material/list";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { FullCalendarModule } from "@fullcalendar/angular";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CalendarioComponent } from "./pages/calendario/calendario.component";
import { SalaListaComponent } from "./pages/sala-lista/sala-lista.component";
import { SalaCriarComponent } from "./pages/sala-criar/sala-criar.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { SalaService } from "./services/sala.service";
import { EventoService } from "./services/evento.service";
import { EventoCriarComponent } from "./pages/evento-criar/evento-criar.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
  declarations: [
    AppComponent,
    CalendarioComponent,
    SalaListaComponent,
    SalaCriarComponent,
    EventoCriarComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    FullCalendarModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [HttpClient, SalaService, EventoService, MatSnackBarModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
