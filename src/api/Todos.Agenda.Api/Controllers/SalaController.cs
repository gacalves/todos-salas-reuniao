﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Api.Dtos;
using Todos.Agenda.Api.Messages;
using Todos.Agenda.Application.Commands.Sala.Atualizar;
using Todos.Agenda.Application.Commands.Sala.Inserir;
using Todos.Agenda.Application.Sala.Remover;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;

namespace Todos.Agenda.Api.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    public class SalaController : ControllerBase {
        private readonly IMediator mediator;
        private readonly ISalaRead salaRead;

        public SalaController(IMediator mediator, ISalaRead salaRead) {
            this.mediator = mediator;
            this.salaRead = salaRead;
        }

        private SalaDto ToDto(Sala sala) {
            return new SalaDto() {
                Nome = sala.Nome,
                SalaId = sala.Id
            };
        }

        [HttpGet]
        public async Task<Response> GetAsync() {
            var salas = await salaRead.ObterSalas();
            var dtos = salas.Select(x => new SalaDto() { Nome = x.Nome, SalaId = x.Id });
            return HttpMessages.Ok(data: dtos);
        }

        [HttpGet("{salaId}")]
        public async Task<Response> Get(Guid salaId) {
            try {
                var sala = await salaRead.ObterSalaPorId(salaId);
                return HttpMessages.Ok("Consulta efetuada com sucesso.", ToDto(sala));
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Não foi possível recuperar a sala informada!", new string[] { ex.Message });
            }
        }

        [HttpPost]
        public async Task<Response> CreateAsync(CriarSalaCommand command) {
            try {
                var result = await mediator.Send(command, CancellationToken.None);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Falha ao criar a sala!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Sala criada com sucesso.");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha inesperada ao criar a sala!", new string[] { ex.Message });
            }
        }

        [HttpPut]
        public async Task<Response> PutAsync(AtualizarSalaCommand command) {
            try {
                var result = await mediator.Send(command, CancellationToken.None);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Falha ao atualizar a sala!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Sala atualizada com sucesso.");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha inesperada ao atualizar a sala!", new string[] { ex.Message });
            }
        }

        [HttpDelete("{salaId}")]
        public async Task<Response> Delete(Guid salaId) {
            try {
                var command = new RemoverSalaCommand() { SalaId = salaId };
                var result = await mediator.Send(command, CancellationToken.None);
                if (result.HasValidation) {
                    return HttpMessages.Failure("Falha ao remover a sala!", result.Validations);
                }
                else {
                    return HttpMessages.Ok("Sala removida com sucesso.");
                }
            }
            catch (Exception ex) {
                return HttpMessages.Failure("Houve uma falha inesperada ao remover a sala!", new string[] { ex.Message });
            }
        }
    }
}
