﻿using Flunt.Notifications;
using Flunt.Validations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Commands.Sala.Atualizar {
    public class AtualizarSalaCommand : Validable, IRequest<Result> {

        public Guid SalaId { get; set; }
        public string Nome { get; set; }

        public override void Validate() {
            AddNotifications(new Contract().IsNotEmpty(SalaId, nameof(SalaId), "Sala inválida!")
                .IsNotNullOrEmpty(Nome, nameof(Nome), "O nome da sala deve conter entre 1 e 250 caracteres")
                .HasMaxLen(Nome, 250, nameof(Nome), "O nome da sala deve no máximo 250 caracteres."));
        }
    }
}
