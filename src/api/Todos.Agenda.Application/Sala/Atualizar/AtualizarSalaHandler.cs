﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Application.Commands;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Services;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Commands.Sala.Atualizar {
    public class AtualizarSalaHandler : IRequestHandler<AtualizarSalaCommand, Result> {
        private readonly IMediator mediator;
        private readonly ISalaRead salaRead;
        private readonly ISalaWrite salaWrite;

        public AtualizarSalaHandler(IMediator mediator, ISalaWrite salaWrite, ISalaRead salaRead) {
            this.mediator = mediator;
            this.salaRead = salaRead;
            this.salaWrite = salaWrite;
        }

        public async Task<Result> Handle(AtualizarSalaCommand request, CancellationToken cancellationToken) {
            return await SalaService.AtualizarSalaAsync(request.SalaId, request.Nome, salaWrite, salaRead, cancellationToken);

        }
    }
}
