import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SalaListaComponent } from "./pages/sala-lista/sala-lista.component";
import { CalendarioComponent } from './pages/calendario/calendario.component';
import { SalaCriarComponent } from './pages/sala-criar/sala-criar.component';
import { EventoCriarComponent } from './pages/evento-criar/evento-criar.component';

const routes: Routes = [
  { path: "sala-lista", component: SalaListaComponent },
  { path: "calendario", component: CalendarioComponent },
  { path: "sala-criar", component: SalaCriarComponent },
  { path: "sala-editar", component: SalaCriarComponent },
  { path: "evento-criar", component: EventoCriarComponent },
  { path: "evento-editar", component: EventoCriarComponent },
  { path: "", redirectTo: "/sala-lista", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
