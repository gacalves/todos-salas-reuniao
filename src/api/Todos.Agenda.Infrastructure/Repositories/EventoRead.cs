﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;

namespace Todos.Agenda.Infrastructure.Repositories {
    public class EventoRead : IEventoRead {
        private AgendaDbContext context;
        public EventoRead(AgendaDbContext context) {
            this.context = context;
        }

        public async Task<int> ContarEventosPorPeriodo(Guid SalaId, DateTime inicio, DateTime fim) {
            return await context.Eventos.Where(x => x.Sala.Id == SalaId && (
            (x.Inicio >= inicio && x.Inicio <= fim) ||
            (x.Fim >= inicio && x.Fim <= fim)
            )).CountAsync();

        }

        public async Task<int> ContarEventosPorPeriodo(Guid SalaId, DateTime inicio, DateTime fim, Guid exceto) {
            return await context.Eventos.Where(x => x.Sala.Id == SalaId
            && ((x.Inicio >= inicio && x.Inicio <= fim) || (x.Fim >= inicio && x.Fim <= fim))
            && x.Id != exceto)
                .CountAsync();
        }

        public async Task<Evento> ObterEventoPorId(Guid eventoId) {
            return await context.Eventos.Include(x => x.Sala).FirstOrDefaultAsync(x => x.Id == eventoId);
        }

        public async Task<IEnumerable<Evento>> ObterEventosPorSala(Guid salaId) {
            var eventos = await context.Eventos.Include(c => c.Sala).Where(x => x.Sala.Id == salaId).ToListAsync();
            return eventos;
        }
    }
}
