﻿using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todos.Agenda.Domain.Entities {
    public class Sala : Notifiable {

        public Guid Id { get; set; }
        public string Nome { get; private set; }
        public bool Ativa { get; set; }

        private Contract GetValidationContract(string nome) {
            return new Contract()
                .IsNotNullOrEmpty(nome, nameof(Nome), "O nome da sala deve ser informado!")
                .HasMaxLen(nome, 250, nameof(Nome), "O nome da sala deve no máximo 250 caracteres!");
        }

        public Sala(string nome, Guid Id = default) {
            AddNotifications(GetValidationContract(nome));
            Nome = nome;
            Ativa = true;
        }

        public Sala() {

        }

        public void Desativar() {
            Ativa = false;
        }

        public void AtualizarNome(string nome) {
            AddNotifications(GetValidationContract(nome));
            Nome = nome;
        }
    }
}
