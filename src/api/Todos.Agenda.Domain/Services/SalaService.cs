﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Entities;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Domain.Services {
    public static class SalaService {

        public static async Task<Result> CriarSalaAsync(string nome, ISalaWrite salaWrite, ISalaRead salaRead, CancellationToken cancellationToken) {
            var sala = new Sala(nome);
            var validations = new Result();

            if (sala.Invalid) {
                foreach (var notification in sala.Notifications)
                    validations.AddValidation(notification.Message);
                return validations;
            }

            var qtdSalasMesmoNome = await salaRead.ContarSalasPorNome(nome);
            if (qtdSalasMesmoNome > 0) {
                validations.AddValidation("Já existe uma sala com este nome!");
                return validations;
            }

            await salaWrite.Inserir(sala, cancellationToken);
            return new Result();
        }


        public static async Task<Result> AtualizarSalaAsync(Guid SalaId, string nome, ISalaWrite salaWrite, ISalaRead salaRead, CancellationToken cancellationToken) {

            var sala = await salaRead.ObterSalaPorId(SalaId);
            var validations = new Result();
            if (sala == null) {
                validations.AddValidation("Sala inválida!");
                return validations;
            }

            var salasMesmoNome = await salaRead.ObterSalasPorNome(nome);
            foreach (var outraSala in salasMesmoNome) {
                if (outraSala.Id != sala.Id) {
                    validations.AddValidation("Outra sala já está utilizando este nome!");
                    return validations;
                }
            }

            sala.AtualizarNome(nome);

            if (sala.Valid) {
                await salaWrite.Atualizar(sala, cancellationToken);
                validations = new Result();
            }
            else {
                foreach (var error in sala.Notifications) {
                    validations.AddValidation(error.Message);
                }
                return validations;
            }
            return validations;
        }

        public static async Task<Result> RemoverSalaAsync(ISalaRead salaRead, ISalaWrite salaWrite, Guid salaId, CancellationToken cancellationToken) {

            var sala = await salaRead.ObterSalaPorId(salaId);
            var result = new Result();
            if (sala == null) {
                result.AddValidation("Sala inválida!");
                return result;
            }

            //Não é interessante remover o registro de eventos da sala questões de histórico.
            var eventos = await salaRead.ContarEventosPorSala(salaId);

            if (eventos > 0) {
                sala.Desativar();
                await salaWrite.Atualizar(sala, cancellationToken);
                return new Result();
            }
            else {
                await salaWrite.Remover(sala.Id, cancellationToken);
                return new Result();
            }

        }
    }
}
