import { Component, OnInit } from "@angular/core";
import { SalaService } from "../../services/sala.service";
import { Sala } from "../../models/sala";
import { Router } from "@angular/router";

@Component({
  selector: "app-sala-lista",
  templateUrl: "./sala-lista.component.html",
  styleUrls: ["./sala-lista.component.css"],
})
export class SalaListaComponent implements OnInit {
  salas: Sala[];

  constructor(private salaService: SalaService, private router: Router) {}

  ngOnInit(): void {
    this.salaService.obterSalas().subscribe((resultado) => {
      this.salas = resultado.data;
    });
  }

  novaSala() {
    this.router.navigate(["sala-criar"]);
  }

  irParaSala(salaId) {
    localStorage.setItem("salaId", salaId);
    this.router.navigate(["calendario"]);
  }

  editarSala(salaId) {
    this.router.navigate(["sala-editar"], { state: { salaId:salaId } });
  }
}
