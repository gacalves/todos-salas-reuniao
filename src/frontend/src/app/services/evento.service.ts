import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ServerResponse } from "../models/server-response";
import { Evento } from '../models/evento';
import { Observable } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
  }),
};

@Injectable({
  providedIn: "root",
})
export class EventoService {
  constructor(private http: HttpClient) {}

  obterEventosPorSala(salaId: string): Observable<ServerResponse<Evento[]>> {
    return this.http.get<ServerResponse<Evento[]>>(
      `${environment.serviceBaseUrl}/Evento/Sala/${salaId}`
    );
  }

  criarEvento(evento) {
    return this.http.post<ServerResponse<Evento>>(
      `${environment.serviceBaseUrl}/Evento`,
      evento,
      httpOptions
    );
  }

  editarEvento(evento) {
    return this.http.put<ServerResponse<any>>(
      `${environment.serviceBaseUrl}/Evento`,
      evento,
      httpOptions
    );
  }

  cancelarEvento(eventoId:string) {
    return this.http.delete<ServerResponse<any>>(
      `${environment.serviceBaseUrl}/Evento/${eventoId}`,
      httpOptions
    );
  }

  obterEventoPorId(eventoId:string) {
    return this.http.get<ServerResponse<Evento>>(
      `${environment.serviceBaseUrl}/Evento/${eventoId}`,
      httpOptions
    );
  }
}
