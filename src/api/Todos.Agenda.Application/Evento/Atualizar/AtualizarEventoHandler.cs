﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todos.Agenda.Domain.Repositories;
using Todos.Agenda.Domain.Services;
using Todos.Agenda.Domain.Validation;

namespace Todos.Agenda.Application.Evento.Atualizar {
    class AtualizarEventoHandler : IRequestHandler<AtualizarEventoCommand, Result> {

        private readonly IEventoRead eventoRead;
        private readonly IEventoWritre eventoWrite;
        private readonly ISalaRead salaRead;

        public AtualizarEventoHandler(IEventoRead eventoRead, IEventoWritre eventoWrite, ISalaRead salaRead) {
            this.eventoRead = eventoRead;
            this.eventoWrite = eventoWrite;
            this.salaRead = salaRead;
        }

        public async Task<Result> Handle(AtualizarEventoCommand request, CancellationToken cancellationToken) {

            return await EventoService.AtualizarEventoAsync(request.EventoId,
                request.Descricao,
                request.Responsavel,
                request.Inicio,
                request.Fim,
                request.SalaId,
                eventoRead, eventoWrite, salaRead, cancellationToken);
        }
    }
}
